package com.brevitaz.pizzaexercise.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Base")
@EntityListeners(AuditingEntityListener.class)
public class Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long baseId;
	
	@NotBlank
	private String baseType;
	
	public Long getBaseId() {
		return baseId;
	}

	public void setBaseId(Long baseId) {
		this.baseId = baseId;
	}

	public String getBaseType() {
		return baseType;
	}

	public void setBaseType(String baseType) {
		this.baseType = baseType;
	}



}
