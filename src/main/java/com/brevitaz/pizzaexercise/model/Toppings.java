package com.brevitaz.pizzaexercise.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Toppings")
@EntityListeners(AuditingEntityListener.class)
public class Toppings {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long toppingsId;
	
	@NotBlank
	private String toppingsName;

	public Long getToppingsId() {
		return toppingsId;
	}

	public void setToppingsId(Long toppingsId) {
		this.toppingsId = toppingsId;
	}

	public String getToppingsName() {
		return toppingsName;
	}

	public void setToppingsName(String toppingsName) {
		this.toppingsName = toppingsName;
	}

}
