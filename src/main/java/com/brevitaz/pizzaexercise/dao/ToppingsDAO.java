package com.brevitaz.pizzaexercise.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.pizzaexercise.model.Toppings;


@Repository
@Transactional
public interface ToppingsDAO  extends CrudRepository<Toppings,Long>{
	List<Toppings> findAll();
}
