package com.brevitaz.pizzaexercise.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.pizzaexercise.model.Base;

@Repository
@Transactional
public interface BaseDAO extends CrudRepository<Base,Long> {
	List<Base> findAll();
}
