package com.brevitaz.pizzaexercise.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.pizzaexercise.model.Pizza;

@Repository
@Transactional
public interface PizzaDAO  extends CrudRepository<Pizza,Long> {
	List<Pizza> findAll();
	
}