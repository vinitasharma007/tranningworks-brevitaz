package com.brevitaz.pizzaexercise.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.pizzaexercise.model.Ingredient;

@Repository
@Transactional
public interface IngredientDAO extends CrudRepository<Ingredient,Long> {
	List<Ingredient> findAll();

	
}
