package com.brevitaz.pizzaexercise.services.pizzaservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brevitaz.pizzaexercise.dao.PizzaDAO;
import com.brevitaz.pizzaexercise.model.Pizza;

@Service
public class PizzaServiceImpl implements PizzaService {

	 @Autowired
	 private PizzaDAO pizzaDAO;
	
	
	@Override
	public List<Pizza> findAll() {
		return pizzaDAO.findAll();
	}

	@Override
	public Pizza save(Pizza pizza) {
		return pizzaDAO.save(pizza);
	}

	@Override
	public Pizza update(Pizza pizza) {
		return pizzaDAO.save(pizza);
	}

	@Override
	public void delete(long pizzaId) {
		pizzaDAO.deleteById(pizzaId);
		
	}

	@Override
	public Optional<Pizza> findByPizzaId(long id) {
		
		return pizzaDAO.findById(id);
	}

}
