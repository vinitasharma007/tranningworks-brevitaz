package com.brevitaz.pizzaexercise.services.pizzaservice;

import java.util.List;
import java.util.Optional;

import com.brevitaz.pizzaexercise.model.Pizza;


public interface PizzaService {
	   
		List<Pizza> findAll();
	    Pizza save(Pizza pizza);
	    Pizza update(Pizza pizza);
	    void delete(long pizzaId);
	    Optional<Pizza> findByPizzaId(long id);

}
