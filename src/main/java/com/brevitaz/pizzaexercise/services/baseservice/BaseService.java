package com.brevitaz.pizzaexercise.services.baseservice;

import java.util.List;
import java.util.Optional;

import com.brevitaz.pizzaexercise.model.Base;


public interface BaseService {
	List<Base> findAll();
	Base save(Base base);
	Base update(Base base);
    void delete(long baseId);
    Optional<Base> findByPizzaId(long id);

}
