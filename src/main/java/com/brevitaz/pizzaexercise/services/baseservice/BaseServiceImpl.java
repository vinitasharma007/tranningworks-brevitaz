package com.brevitaz.pizzaexercise.services.baseservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brevitaz.pizzaexercise.dao.BaseDAO;
import com.brevitaz.pizzaexercise.model.Base;

@Service
public class BaseServiceImpl implements BaseService{

	@Autowired
	private BaseDAO baseDAO;
	
	@Override
	public List<Base> findAll() {
		return baseDAO.findAll();
	}

	@Override
	public Base save(Base base) {
		return baseDAO.save(base);
	}

	@Override
	public Base update(Base base) {
		return baseDAO.save(base);
	}
	@Override
	public void delete(long baseId) {
		baseDAO.deleteById(baseId);
	}

	@Override
	public Optional<Base> findByPizzaId(long id) {
		return baseDAO.findById(id);
	}
	
	

}
