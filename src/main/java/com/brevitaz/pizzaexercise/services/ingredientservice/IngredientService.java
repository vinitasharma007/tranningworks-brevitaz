package com.brevitaz.pizzaexercise.services.ingredientservice;

import java.util.List;
import java.util.Optional;

import com.brevitaz.pizzaexercise.model.Ingredient;


public interface IngredientService {
	List<Ingredient> findAll();
	Ingredient save(Ingredient ingredient);
	Ingredient update(Ingredient ingredient);
    void delete(long ingredientId);
    Optional<Ingredient> findByPizzaId(long id);
}
