package com.brevitaz.pizzaexercise.services.ingredientservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brevitaz.pizzaexercise.dao.IngredientDAO;
import com.brevitaz.pizzaexercise.model.Ingredient;

@Service
public class IngredientServiceImpl implements IngredientService{

	@Autowired
	private  IngredientDAO ingredientDAO;
	@Override
	public List<Ingredient> findAll() {
		return ingredientDAO.findAll();
	}

	@Override
	public Ingredient save(Ingredient ingredient) {
		return ingredientDAO.save(ingredient);
	}

	@Override
	public Ingredient update(Ingredient ingredient) {
		return ingredientDAO.save(ingredient);
	}

	@Override
	public void delete(long ingredientId) {
		ingredientDAO.deleteById(ingredientId);
		
	}

	@Override
	public Optional<Ingredient> findByPizzaId(long id) {
		return ingredientDAO.findById(id);
	}

}
