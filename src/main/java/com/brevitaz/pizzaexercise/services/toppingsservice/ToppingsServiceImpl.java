package com.brevitaz.pizzaexercise.services.toppingsservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brevitaz.pizzaexercise.dao.ToppingsDAO;
import com.brevitaz.pizzaexercise.model.Toppings;

@Service
public class ToppingsServiceImpl implements ToppingsService {

	@Autowired
	private ToppingsDAO toppingsDAO;
	
	@Override
	public List<Toppings> findAll() {
		return toppingsDAO.findAll();
	}

	@Override
	public Toppings save(Toppings toppings) {
		return toppingsDAO.save(toppings);
	}

	@Override
	public Toppings update(Toppings toppings) {
		return toppingsDAO.save(toppings);
	}

	@Override
	public void delete(long toppingsId) {
		toppingsDAO.deleteById(toppingsId);
	}

	@Override
	public Optional<Toppings> findByPizzaId(long id) {
		return toppingsDAO.findById(id);
	}

}
