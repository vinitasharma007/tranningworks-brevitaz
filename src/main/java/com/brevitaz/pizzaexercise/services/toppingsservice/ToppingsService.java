package com.brevitaz.pizzaexercise.services.toppingsservice;

import java.util.List;
import java.util.Optional;

import com.brevitaz.pizzaexercise.model.Toppings;

public interface ToppingsService {
	List<Toppings> findAll();
	Toppings save(Toppings toppings);
	Toppings update(Toppings toppings);
    void delete(long toppingsId);
    Optional<Toppings> findByPizzaId(long id);

}
