package com.brevitaz.pizzaexercise.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brevitaz.pizzaexercise.model.Pizza;



public interface PizzaRepository extends JpaRepository<Pizza, Long> {

}
