package com.brevitaz.pizzaexercise.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.brevitaz.pizzaexercise.controller.BaseController;
import com.brevitaz.pizzaexercise.controller.IngredientController;
import com.brevitaz.pizzaexercise.controller.PizzaController;
import com.brevitaz.pizzaexercise.controller.ToppingsController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@PropertySource("classpath:swagger.properties")
@ComponentScan(basePackageClasses={PizzaController.class,BaseController.class,IngredientController.class,ToppingsController.class})
@EnableSwagger2
@SpringBootApplication
public class SwaggerConfig{

     @Bean
    public Docket api() {
    	 return new Docket(DocumentationType.SWAGGER_2)
    			 .select()
                 .apis(RequestHandlerSelectors.any())           
                 .paths(PathSelectors.any())
                 .build();
    }
   
}