package com.brevitaz.pizzaexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class OrderPizzaMain {

	public static void main(String[] args) {
		
		SpringApplication.run(OrderPizzaMain.class, args);

	}

}
