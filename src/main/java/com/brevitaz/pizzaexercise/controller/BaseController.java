package com.brevitaz.pizzaexercise.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brevitaz.pizzaexercise.model.Base;
import com.brevitaz.pizzaexercise.services.baseservice.BaseService;


import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController  
@CrossOrigin(value = "/base")
@Api(value="BaseController", produces=MediaType.APPLICATION_JSON_VALUE)
public class BaseController {
	@Autowired
	 private BaseService baseService;

	  
	 @RequestMapping(value = "saveBase",method = RequestMethod.POST)     // or user @GetMapping
	 public Base save(@RequestBody Base base){
	     return baseService.save(base);
	 }

	 @RequestMapping(value = "updateBase",method = RequestMethod.POST)     // or user @GetMapping
	 public Base update(@RequestBody Base base){
	     return baseService.update(base);
	 }
	 
	 
	 @RequestMapping(value = "findBaseById",method = RequestMethod.GET)   // or use @GetMapping
	 public Optional<Base> findById(@RequestParam long id) {
	     return baseService.findByPizzaId(id);
	 }

   @RequestMapping(value = "listBase",method = RequestMethod.GET)   // or use @GetMapping
	public java.util.List<Base> listBase() {
	    return baseService.findAll();
	}

	@RequestMapping(value = "deleteBase", method = RequestMethod.DELETE)  // or use @DeleteMapping
	public void delete(@RequestParam("id")long id){
		baseService.delete(id);
	}


}
