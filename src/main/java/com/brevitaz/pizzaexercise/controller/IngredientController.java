package com.brevitaz.pizzaexercise.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brevitaz.pizzaexercise.model.Ingredient;
import com.brevitaz.pizzaexercise.services.ingredientservice.IngredientService;


import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController  
@CrossOrigin(value = "/ingredient")
@Api(value="IngredientController", produces=MediaType.APPLICATION_JSON_VALUE)
public class IngredientController {
	 @Autowired
	 private IngredientService ingredientService;

	  
	 @RequestMapping(value = "saveIngredient",method = RequestMethod.POST)     // or user @GetMapping
	 public Ingredient save(@RequestBody Ingredient ingredient){
	     return ingredientService.save(ingredient);
	 }

	 @RequestMapping(value = "updateIngredient",method = RequestMethod.POST)     // or user @GetMapping
	 public Ingredient update(@RequestBody Ingredient ingredient){
	     return ingredientService.update(ingredient);
	 }
	 
	 
	 @RequestMapping(value = "findIngredientById",method = RequestMethod.GET)   // or use @GetMapping
	 public Optional<Ingredient> findById(@RequestParam long id) {
	     return ingredientService.findByPizzaId(id);
	 }

   @RequestMapping(value = "listIngredient",method = RequestMethod.GET)   // or use @GetMapping
	public java.util.List<Ingredient> listIngredient() {
	    return ingredientService.findAll();
	}

	@RequestMapping(value = "deleteIngredient", method = RequestMethod.DELETE)  // or use @DeleteMapping
	public void delete(@RequestParam("id")long id){
		ingredientService.delete(id);
	}


}
