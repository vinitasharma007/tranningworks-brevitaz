package com.brevitaz.pizzaexercise.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brevitaz.pizzaexercise.model.Pizza;
import com.brevitaz.pizzaexercise.services.pizzaservice.PizzaService;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController  
@CrossOrigin(value = "/pizza")
@Api(value="PizzaController", produces=MediaType.APPLICATION_JSON_VALUE)
public class PizzaController {
	 @Autowired
	 private PizzaService pizzaService;

	  
	 @RequestMapping(value = "savePizza",method = RequestMethod.POST)     // or user @GetMapping
	 public Pizza save(@RequestBody Pizza pizza){
	     return pizzaService.save(pizza);
	 }

	 @RequestMapping(value = "updatePizza",method = RequestMethod.POST)     // or user @GetMapping
	 public Pizza update(@RequestBody Pizza pizza){
	     return pizzaService.update(pizza);
	 }
	 
	 
	 @RequestMapping(value = "findPizzaById",method = RequestMethod.GET)   // or use @GetMapping
	 public Optional<Pizza> findById(@RequestParam long id) {
	     return pizzaService.findByPizzaId(id);
	 }

    @RequestMapping(value = "listPizza",method = RequestMethod.GET)   // or use @GetMapping
	public java.util.List<Pizza> listPizza() {
	    return pizzaService.findAll();
	}

	@RequestMapping(value = "deletePizza", method = RequestMethod.DELETE)  // or use @DeleteMapping
	public void delete(@RequestParam("id")long id){
	 	pizzaService.delete(id);
	}

}
