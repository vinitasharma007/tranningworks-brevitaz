package com.brevitaz.pizzaexercise.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brevitaz.pizzaexercise.model.Toppings;
import com.brevitaz.pizzaexercise.services.toppingsservice.ToppingsService;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController  
@CrossOrigin(value = "/toppings")
@Api(value="ToppingsController", produces=MediaType.APPLICATION_JSON_VALUE)
public class ToppingsController {
	@Autowired
	 private ToppingsService toppingsService;

	  
	 @RequestMapping(value = "saveToppings",method = RequestMethod.POST)     // or user @GetMapping
	 public Toppings save(@RequestBody Toppings toppings){
	     return toppingsService.save(toppings);
	 }

	 @RequestMapping(value = "updateToppings",method = RequestMethod.POST)     // or user @GetMapping
	 public Toppings update(@RequestBody Toppings toppings){
	     return toppingsService.update(toppings);
	 }
	 
	 
	 @RequestMapping(value = "findToppingsById",method = RequestMethod.GET)   // or use @GetMapping
	 public Optional<Toppings> findById(@RequestParam long id) {
	     return toppingsService.findByPizzaId(id);
	 }

  @RequestMapping(value = "listToppings",method = RequestMethod.GET)   // or use @GetMapping
	public java.util.List<Toppings> listToppings() {
	    return toppingsService.findAll();
	}

	@RequestMapping(value = "deleteToppings", method = RequestMethod.DELETE)  // or use @DeleteMapping
	public void delete(@RequestParam("id")long id){
		toppingsService.delete(id);
	}


}
